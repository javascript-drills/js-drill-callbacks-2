// Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

//     Get information from the Thanos boards
//     Get all the lists for the Thanos board
//     Get all cards for the Mind list simultaneously
const boardInformation = require("./callback1.cjs"); 
const returnList = require("./callback2.cjs"); 
const returnCards = require("./callback3.cjs");

const boards = require('./boards.json');
const listInfo = require('./lists.json');
const cardsInfo = require("./cards.json")

try{
    function useFunctions(thanosId){
        boardInformation(boards, thanosId, (err, data) => {
            if(err){
                console.log(err);
            }
            else{
                console.log(data);

                returnList(listInfo, data[0].id, (err, data2) => {
                    if(err){
                        console.error(err);
                    }
                    else{
                        console.log(data2);

                        let key = data2.find((value) => value.name === "Mind");

                        returnCards(cardsInfo, key.id, (err, data3) => {
                            if(err){
                                console.log(err);
                            }
                            else{
                                console.log(data3);
                            }
                        });
                    }
                });
            }
        });
    }
}catch(err){
    console.error(err);
}

module.exports = useFunctions;