// Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. 
// Then pass control back to the code that called it by using a callback function.

function returnList( listInfo, boardId, callback){
    try{
        setTimeout( () => {
    
            let key = Object.keys(listInfo)
            .find( id => id === boardId);
            
            callback(null, listInfo[key]);
            
        }, 2000);
    }catch(err){
        console.error(err);
    }
}

module.exports = returnList;