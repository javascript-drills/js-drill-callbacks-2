// Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json 
// and then pass control back to the code that called it by using a callback function.

function boardInformation(boardsInfo, boardID, callback){
    try{
        setTimeout( () => {
            let boardData = boardsInfo.filter( (data) => {
                return data.id === boardID
            });
    
            callback(null, (boardData));
        }, 2*1000);
    }catch(err){
        console.error(err);
    }
}

module.exports = boardInformation;