// Problem 5: Write a function that will use the previously written functions to get the following information. 
// You do not need to pass control back to the code that called it.

//     Get information from the Thanos boards
//     Get all the lists for the Thanos board
//     Get all cards for the Mind and Space lists simultaneously

const boardInformation = require("./callback1.cjs"); 
const returnList = require("./callback2.cjs"); 
const returnCards = require("./callback3.cjs");

const boards = require('./boards.json');
const listInfo = require('./lists.json');
const cardsInfo = require("./cards.json")

try{
    function useFunctions(thanosId){
        boardInformation(boards, thanosId, (err, data) => {
            if(err){
                console.log(err);
            }
            else{
                console.log(data);
                
                returnList(listInfo, data[0].id, (err, data2) => {
                    if(err){
                        console.error(err);
                    }
                    else{
                        console.log(data2);
                        
                        let keys = data2.reduce((acc, current) => {
                            if(current.name === 'Mind' || current.name === 'Space'){
                                acc.push(current.id);
                            }
                            return acc;
                        }, [])

                        keys.forEach( (eachKey) => {
                            returnCards(cardsInfo, eachKey, (err, data3) => {
                                if(err){
                                    console.log(err);
                                }
                                else{
                                    console.log(data3);
                                }
                            })
                        })
                    }
                });
            }
        });
    }
}catch(err){
    console.log(err);
}

module.exports = useFunctions;